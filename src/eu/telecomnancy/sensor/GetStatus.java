package eu.telecomnancy.sensor;

public class GetStatus implements Commande {

	private ISensor sensor;
	
	public GetStatus(ISensor sensor) {
		// TODO Auto-generated constructor stub
		this.sensor = sensor;
	}

	@Override
	public void execute() {
		this.sensor.getStatus();
	}

}
