package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Observer;
import java.util.Random;

public class TemperatureSensorEtat implements ISensor {
    private Etat etat;
    double value = 0;
    private ArrayList<Observer> lo = new ArrayList<>();

    public TemperatureSensorEtat() {
		this.etat = new EtatOff(this);
	}
    
    public void setEtat(Etat e){
    	this.etat = e;
    }
    
    @Override
    public void on() {
    	this.etat.on();
    }

    @Override
    public void off() {
    	this.etat.off();
    }



    @Override
    public void update() throws SensorNotActivatedException {
        etat.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return etat.getValue();
    }
    
    public void addObserver(Observer o){
		this.lo.add(o);
	}
	
	public void deleteObserver(Observer o){
		this.lo.remove(o);
	}
	
	public void notifyObservers(){
		for(int i = 0 ; i < this.lo.size() ; i++){
			this.lo.get(i).update(null, null);
		}
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return false;
	}

}
