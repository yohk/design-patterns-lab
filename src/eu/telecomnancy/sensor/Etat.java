package eu.telecomnancy.sensor;

public abstract class Etat {
	protected TemperatureSensorEtat sensor;
	
	public Etat(TemperatureSensorEtat temperatureSensorEtat){
		this.sensor = temperatureSensorEtat;
	}


	public TemperatureSensorEtat getSensor() {
		return sensor;
	}

	public void setSensor(TemperatureSensorEtat sensor) {
		this.sensor = sensor;
	}

	public abstract void on();
	
	public abstract void off();

	public abstract void update() throws SensorNotActivatedException;

	public abstract double getValue() throws SensorNotActivatedException;

}
