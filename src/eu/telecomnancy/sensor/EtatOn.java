package eu.telecomnancy.sensor;

import java.util.Random;

public class EtatOn extends Etat {

	public EtatOn(TemperatureSensorEtat sensor) {
		super(sensor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void on() {
		
	}

	@Override
	public void off() {
		super.getSensor().setEtat(new EtatOff(sensor));
	}
	
	@Override
    public void update(){
            super.getSensor().value = (new Random()).nextDouble() * 100;
    }
	
	@Override
    public double getValue(){
            return super.getSensor().value;
    }

}
