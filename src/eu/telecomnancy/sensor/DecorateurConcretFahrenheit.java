package eu.telecomnancy.sensor;

public class DecorateurConcretFahrenheit extends Decorateur {
	
	public DecorateurConcretFahrenheit(ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return 1.8*this.sensor.getValue() + 32;
	}

}
