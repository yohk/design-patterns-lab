package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class LegacyAdapter implements ISensor {
	
	private LegacyTemperatureSensor lt; 
	private ArrayList<Observer> lo;
	
	
	public LegacyAdapter() {
		this.lt = new LegacyTemperatureSensor();
		this.lo = new ArrayList<>();
	}

	@Override
	public void on() {
		if(!this.getStatus()){
			this.lt.onOff();
		}
	}

	@Override
	public void off() {
		if(this.getStatus()){
			this.lt.onOff();
		}
	}

	@Override
	public boolean getStatus() {
		return this.lt.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {

	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if(this.getStatus()){
			return this.lt.getTemperature();
		}else{
			throw new SensorNotActivatedException("Erreur");
		}
	}
	
	public void addObserver(Observer o){
		this.lo.add(o);
	}
	
	public void deleteObserver(Observer o){
		this.lo.remove(o);
	}
	
	public void notifyObservers(){
		for(int i = 0 ; i < this.lo.size() ; i++){
			this.lo.get(i).update(null, null);
		}
	}
}
