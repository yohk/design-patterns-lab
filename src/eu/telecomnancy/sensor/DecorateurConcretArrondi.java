package eu.telecomnancy.sensor;

public class DecorateurConcretArrondi extends Decorateur {
	
	public DecorateurConcretArrondi(ISensor sensor) {
		this.sensor = sensor;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return (int)this.sensor.getValue();
	}

}
