package eu.telecomnancy.sensor;

import java.util.Date;
import java.util.Observer;

public class ISensorProxy implements ISensor {
	
	private ISensor sensor;
	
	public ISensorProxy(ISensor s) {
		this.sensor = s;
	}

	@Override
	public void on() {
		// TODO Auto-generated method stub
		System.out.println((new Date().toString()) + " on");
		this.sensor.on();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		System.out.println((new Date().toString()) + " off");
		this.sensor.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		System.out.println((new Date().toString()) + " getStatus");
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		System.out.println((new Date().toString()) + " update");
		this.sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		System.out.println((new Date().toString()) + " getValue");
		return this.sensor.getValue();
	}

	@Override
	public void addObserver(Observer o) {
		// TODO Auto-generated method stub
		this.sensor.addObserver(o);
	}

	@Override
	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub
		this.sensor.deleteObserver(o);
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		this.sensor.notifyObservers();
	}

}
