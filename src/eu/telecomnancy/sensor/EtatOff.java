package eu.telecomnancy.sensor;

import java.util.Random;

public class EtatOff extends Etat{

	public EtatOff(TemperatureSensorEtat temperatureSensorEtat) {
		super(temperatureSensorEtat);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void on() {
		super.getSensor().setEtat(new EtatOn(sensor));
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public void update() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }
	
	@Override
    public double getValue() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
