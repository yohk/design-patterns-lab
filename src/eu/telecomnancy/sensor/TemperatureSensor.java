package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Observer;
import java.util.Random;

public class TemperatureSensor implements ISensor {
    boolean state;
    double value = 0;
    private ArrayList<Observer> lo = new ArrayList<>();

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    public void addObserver(Observer o){
		this.lo.add(o);
	}
	
	public void deleteObserver(Observer o){
		this.lo.remove(o);
	}
	
	public void notifyObservers(){
		for(int i = 0 ; i < this.lo.size() ; i++){
			this.lo.get(i).update(null, null);
		}
	}

}