package eu.telecomnancy.sensor;

import java.util.Observer;

public abstract class Decorateur implements ISensor {
	
	protected ISensor sensor;


	@Override
	public void on() {
		this.sensor.on();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		this.sensor.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return this.sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		this.sensor.update();
	}

	@Override
	public abstract double getValue() throws SensorNotActivatedException;

	@Override
	public void addObserver(Observer o) {
		// TODO Auto-generated method stub
		this.sensor.addObserver(o);
	}

	@Override
	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub
		this.sensor.deleteObserver(o);
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		this.sensor.notifyObservers();
	}

}
