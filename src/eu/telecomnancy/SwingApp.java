package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ISensorProxy;
import eu.telecomnancy.sensor.LegacyAdapter;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        //ISensor sensor = new TemperatureSensor();
        ISensor sensor = new LegacyAdapter();
    	new MainWindow(new ISensorProxy(sensor));
    }

}
