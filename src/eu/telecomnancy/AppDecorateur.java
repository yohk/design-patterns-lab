

package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurConcretArrondi;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecorateur {
	 public static void main(String[] args) {
	        ISensor sensor = new TemperatureSensor();
	        DecorateurConcretArrondi dec = new DecorateurConcretArrondi(sensor);
	        new ConsoleUI(dec);
	    }
}

