package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensorEtat;
import eu.telecomnancy.ui.ConsoleUI;

public class AppEtat {

    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensorEtat();
        new ConsoleUI(sensor);
    }
}