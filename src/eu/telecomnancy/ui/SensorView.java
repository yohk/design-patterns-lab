package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.GetStatus;
import eu.telecomnancy.sensor.GetValue;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyAdapter;
import eu.telecomnancy.sensor.Off;
import eu.telecomnancy.sensor.On;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.Update;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private Off offC;
    private On onC;
    private GetValue gvC;
    private GetStatus gsC;
    private Update up;
    

    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());
        this.offC= new Off(this.sensor);
        this.onC = new On(this.sensor);
        this.gsC = new GetStatus(this.sensor);
        this.gvC = new GetValue(this.sensor);
        this.up = new Update(this.sensor);
        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onC.execute();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                offC.execute();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                up.execute();
                sensor.notifyObservers();
            }
        });
        this.sensor.addObserver(this);
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void update(Observable arg0, Object arg1) {
		try {
			this.value.setText(this.sensor.getValue()+"");
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
